<?php

class Pelanggan_Model extends CI_Model {
    
    public function show_data() {
        return $this->db->get('pelanggan');
    }
    public function input_data($data){
        $this->db->insert('pelanggan', $data);
    }
    public function delete_data($where, $table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function edit_data($where, $table){
        return $this->db->get_where($table, $where);
    }
    public function update_data($where, $data, $table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }
    public function detail_data($id_pelanggan = NULL){
        $query = $this->db->get_where('pelanggan', array('id_pelanggan' => $id_pelanggan))->row();
        return $query;
    }
}
