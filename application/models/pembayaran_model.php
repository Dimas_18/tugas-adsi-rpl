<?php

class Pembayaran_Model extends CI_Model {
    public function show_data() {
        return $this->db->get('pembayaran');
    }
    public function input_data($data){
        $this->db->insert('pembayaran', $data);
    }
    public function delete_data($where, $table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    public function edit_data($where, $table){
        return $this->db->get_where($table, $where);
    }
    public function update_data($where, $data, $table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }
    public function detail_data($id_pembayaran = NULL){
        $query = $this->db->get_where('pembayaran', array('id_pembayaran' => $id_pembayaran))->row();
        return $query;
    }
}
