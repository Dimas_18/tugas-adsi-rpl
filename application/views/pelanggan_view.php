<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pelanggan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pelanggan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-plus"></i> Tambah Data Pelanggan
        </button>
        <table class="table">
            <tr>
                <th>ID PELANGGAN</th>
                <th>NAMA PELANGGAN</th>
                <th>ALAMAT</th>
                <th>NO TELP</th>
                <th>KOMENTAR</th>
                <th>ID PELAYANAN</th>
                <th colspan="2">Action</th>
            </tr>
            <?php $no = 1; foreach ($pelanggan as $customer) : ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $customer->nama_pelanggan ?></td>
                    <td><?php echo $customer->alamat ?></td>
                    <td><?php echo $customer->no_telp ?></td>
                    <td><?php echo $customer->komentar ?></td>
                    <td><?php echo $customer->id_pelayanan ?></td>
                    <td><?php echo anchor('pelanggan_controller/detail/'.$customer->id_pelanggan, '<div class="btn btn-success btn-sm"><i class="fa fa-search-plus"></i> Detail</div>') ?></td>
                    <td><?php echo anchor('pelanggan_controller/edit/'.$customer->id_pelanggan, '<div class="btn btn-primary btn-sm"><i class="fa fa-edit"> Edit</i></div>') ?></td>
                    <td onclick="javascript: return confirm('Anda yakin ingin menghapus data ini ?') "><?php echo anchor('pelanggan_controller/delete/'.$customer->id_pelanggan, '<div class="btn btn-danger btn-sm"><i class="fa fa-trash"> Hapus</i></div>') ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Data Pelanggan</h5>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url().'pelanggan_controller/add_action'; ?>">
            <div class="form-group">
                <label>Nama Pelanggan</label><input type="text" name="nama_pelanggan" class="form-control">
            </div>
            <div class="form-group">
                <label>Alamat</label><input type="text" name="alamat" class="form-control">
            </div>
            <div class="form-group">
                <label>No Telp</label><input type="number" name="no_telp" class="form-control">
            </div>
            <div class="form-group">
                <label>Komentar</label><input type="text" name="komentar" class="form-control">
            </div>
            <div class="form-group">
                <label>Id Pelayanan</label><input type="int" name="id_pelayanan" class="form-control">
            </div>
            <button type="reset" class="btn btn-danger" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
