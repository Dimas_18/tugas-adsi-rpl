<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pembayaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pembayaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-plus"></i> Tambah Data Pembayaran
        </button>
        <table class="table">
            <tr>
                <th>ID PEMBAYARAN</th>
                <th>TOTAL BAYAR</th>
                <th>METODE PEMBAYARAN</th>
                <th>TANGGAL PEMBAYARAN</th>
                <th>STATUS PEMBAYARAN</th>
                <th>ID PELANGGAN</th>
                <th colspan="2">Action</th>
            </tr>
            <?php $no = 1; foreach ($pembayaran as $pay) : ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $pay->total_bayar ?></td>
                    <td><?php echo $pay->metode_pembayaran ?></td>
                    <td><?php echo $pay->tanggal_pembayaran ?></td>
                    <td><?php echo $pay->status_pembayaran ?></td>
                    <td><?php echo $pay->id_pelanggan ?></td>
                    <td><?php echo anchor('pembayaran_controller/detail/'.$pay->id_pembayaran, '<div class="btn btn-success btn-sm"><i class="fa fa-search-plus"></i> Detail</div>') ?></td>
                    <td><?php echo anchor('pembayaran_controller/edit/'.$pay->id_pembayaran, '<div class="btn btn-primary btn-sm"><i class="fa fa-edit"> Edit</i></div>') ?></td>
                    <td onclick="javascript: return confirm('Anda yakin ingin menghapus data ini ?') "><?php echo anchor('pembayaran_controller/delete/'.$pay->id_pembayaran, '<div class="btn btn-danger btn-sm"><i class="fa fa-trash"> Hapus</i></div>') ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Data Pembayaran</h5>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url().'pembayaran_controller/add_action'; ?>">
            <div class="form-group">
                <label>Total Bayar</label><input type="number" name="total_bayar" class="form-control">
            </div>
            <div class="form-group">
                <label>Metode Pembayaran</label><input type="bool" name="metode_pembayaran" class="form-control">
            </div>
            <div class="form-group">
                <label>Tanggal Pembayaran</label><input type="date" name="tanggal_pembayaran" class="form-control">
            </div>
            <div class="form-group">
                <label>Status Pembayaran</label><input type="bool" name="status_pembayaran" class="form-control">
            </div>
            <div class="form-group">
                <label>Id Pelanggan</label><input type="int" name="id_pelanggan" class="form-control">
            </div>
            <button type="reset" class="btn btn-danger" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
