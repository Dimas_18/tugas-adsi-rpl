<div class="content-wrapper">
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pembayaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pembayaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <h4><strong>DETAIL DATA Pembayaran</strong></h4>
    </section>
    <table class="table">
        <tr>
            <th>TOTAL BAYAR</th>
            <td><?php echo $detail->total_bayar ?></td>
        </tr>
        <tr>
            <th>METODE PEMBAYARAN</th>
            <td><?php echo $detail->metode_pembayaran ?></td>
        </tr>
        <tr>
            <th>TANGGAL PEMBAYARAN</th>
            <td><?php echo $detail->tanggal_pembayaran ?></td>
        </tr>
        <tr>
            <th>STATUS PEMBAYARAN</th>
            <td><?php echo $detail->status_pembayaran ?></td>
        </tr>
        <tr>
            <th>ID PELANGGAN</th>
            <td><?php echo $detail->id_pelanggan ?></td>
        </tr>
    </table>
    <a href="<?php echo base_url('pembayaran_controller/index'); ?>" class="btn btn-primary">Kembali</a>
</div>