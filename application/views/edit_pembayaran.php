<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pembayaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pembayaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <?php foreach($pembayaran as $pay) { ?>
        <form action="<?php echo base_url().'pembayaran_controller/update'; ?>" method="post">
            <div class="form-group">
                <label>Total Bayar</label><input type="hidden" name="id_pembayaran" class="form-control" value="<?php echo $pay->id_pembayaran ?>"><input type="number" name="total_bayar" class="form-control" value="<?php echo $pay->total_bayar ?>">
            </div>
            <div class="form-group">
                <label>Metode Pembayaran</label><input type="bool" name="metode_pembayaran" class="form-control" value="<?php echo $pay->metode_pembayaran ?>">
            </div>
            <div class="form-group">
                <label>Tanggal Pembayaran</label><input type="date" name="tanggal_pembayaran" class="form-control" value="<?php echo $pay->tanggal_pembayaran ?>">
            </div>
            <div class="form-group">
                <label>Status Pembayaran</label><input type="bool" name="status_pembayaran" class="form-control" value="<?php echo $pay->status_pembayaran ?>">
            </div>
            <div class="form-group">
                <label>Id Pelanggan</label><input type="int" name="id_pelanggan" class="form-control" value="<?php echo $pay->id_pelanggan ?>">
            </div>
            <button type="reset" class="btn btn-danger">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
        <?php } ?>
    </section>
</div>