<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pelanggan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pelanggan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <?php foreach($pelanggan as $customer) { ?>
        <form action="<?php echo base_url().'pelanggan_controller/update'; ?>" method="post">
            <div class="form-group">
                <label>Nama Pelanggan</label><input type="hidden" name="id_pelanggan" class="form-control" value="<?php echo $customer->id_pelanggan ?>"><input type="text" name="nama_pelanggan" class="form-control" value="<?php echo $customer->nama_pelanggan ?>">
            </div>
            <div class="form-group">
                <label>Alamat</label><input type="text" name="alamat" class="form-control" value="<?php echo $customer->alamat ?>">
            </div>
            <div class="form-group">
                <label>No Telp</label><input type="number" name="no_telp" class="form-control" value="<?php echo $customer->no_telp ?>">
            </div>
            <div class="form-group">
                <label>Komentar</label><input type="text" name="komentar" class="form-control" value="<?php echo $customer->komentar ?>">
            </div>
            <div class="form-group">
                <label>Id Pelayanan</label><input type="int" name="id_pelayanan" class="form-control" value="<?php echo $customer->id_pelayanan ?>">
            </div>
            <button type="reset" class="btn btn-danger">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
        <?php } ?>
    </section>
</div>