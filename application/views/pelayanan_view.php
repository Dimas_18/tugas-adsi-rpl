<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pelayanan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pelayanan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-plus"></i> Tambah Data Pelayanan
        </button>
        <table class="table">
            <tr>
                <th>ID PELAYANAN</th>
                <th>JENIS PELAYANAN</th>
                <th>HARGA</th>
                <th>WAKTU PELAYANAN</th>
                <th colspan="2">Action</th>
            </tr>
            <?php $no = 1; foreach ($pelayanan as $service) : ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $service->jenis_pelayanan ?></td>
                    <td><?php echo $service->harga ?></td>
                    <td><?php echo $service->waktu_pelayanan ?></td>
                    <td><?php echo anchor('pelayanan_controller/detail/'.$service->id_pelayanan, '<div class="btn btn-success btn-sm"><i class="fa fa-search-plus"></i> Detail</div>') ?></td>
                    <td><?php echo anchor('pelayanan_controller/edit/'.$service->id_pelayanan, '<div class="btn btn-primary"><i class="fa fa-edit"> Edit</i></div>') ?></td>
                    <td onclick="javascript: return confirm('Anda yakin ingin menghapus data ini ?') "><?php echo anchor('pelayanan_controller/delete/'.$service->id_pelayanan, '<div class="btn btn-danger"><i class="fa fa-trash"> Hapus</i></div>') ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Data Pelayanan</h5>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url().'pelayanan_controller/add_action'; ?>">
            <div class="form-group">
                <label>Jenis Pelayanan</label>
                <select name="jenis_pelayanan" class="form-control">
                  <option>Pengecatan Baru</option>
                  <option>Pengecatan Ulang</option>
                  <option>Repair Warna Cat</option>
                </select>
            </div>
            <div class="form-group">
                <label>Harga</label><input type="number" name="harga" class="form-control">
            </div>
            <div class="form-group">
                <label>Waktu Pelayanan</label><input type="time" name="waktu_pelayanan" class="form-control">
            </div>
            <button type="reset" class="btn btn-danger" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
