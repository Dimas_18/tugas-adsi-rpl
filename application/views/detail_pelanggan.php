<div class="content-wrapper">
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pelanggan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pelanggan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <h4><strong>DETAIL DATA PELANGGAN</strong></h4>
    </section>
    <table class="table">
        <tr>
            <th>NAMA PELANGGAN</th>
            <td><?php echo $detail->nama_pelanggan ?></td>
        </tr>
        <tr>
            <th>ALAMAT</th>
            <td><?php echo $detail->alamat ?></td>
        </tr>
        <tr>
            <th>NO TELP</th>
            <td><?php echo $detail->no_telp ?></td>
        </tr>
        <tr>
            <th>KOMENTAR</th>
            <td><?php echo $detail->komentar ?></td>
        </tr>
        <tr>
            <th>ID PELAYANAN</th>
            <td><?php echo $detail->id_pelayanan ?></td>
        </tr>
    </table>
    <a href="<?php echo base_url('pelanggan_controller/index'); ?>" class="btn btn-primary">Kembali</a>
</div>