<div class="content-wrapper">
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pelayanan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pelayanan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <h4><strong>DETAIL DATA PELAYANAN</strong></h4>
    </section>
    <table class="table">
        <tr>
            <th>JENIS PELAYANAN</th>
            <td><?php echo $detail->jenis_pelayanan ?></td>
        </tr>
        <tr>
            <th>HARGA</th>
            <td><?php echo $detail->harga ?></td>
        </tr>
        <tr>
            <th>WAKTU PELAYANAN</th>
            <td><?php echo $detail->waktu_pelayanan ?></td>
        </tr>
    </table>
    <a href="<?php echo base_url('pelayanan_controller/index'); ?>" class="btn btn-primary">Kembali</a>
</div>