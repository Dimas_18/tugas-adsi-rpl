<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Pelayanan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pelayanan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <?php foreach($pelayanan as $service) { ?>
        <form action="<?php echo base_url().'pelayanan_controller/update'; ?>" method="post">
            <div class="form-group">
                <label>Jenis Pelayanan</label><input type="hidden" name="id_pelayanan" class="form-control" value="<?php echo $service->id_pelayanan ?>">
                <select name="jenis_pelayanan" class="form-control" value="<?php echo $service->jenis_pelayanan ?>">
                  <option>Pengecatan Baru</option>
                  <option>Pengecatan Ulang</option>
                  <option>Repair Warna Cat</option>
                </select>
            </div>
            <div class="form-group">
                <label>Harga</label><input type="number" name="harga" class="form-control" value="<?php echo $service->harga ?>">
            </div>
            <div class="form-group">
                <label>Waktu Pelayanan</label><input type="time" name="waktu_pelayanan" class="form-control" value="<?php echo $service->waktu_pelayanan ?>">
            </div>
            <button type="reset" class="btn btn-danger">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
        <?php } ?>
    </section>
</div>