<?php

class Pelanggan_Controller extends CI_Controller {

	public function index()
	{
		$this->load->model('pelanggan_model');
		$data['pelanggan'] = $this->pelanggan_model->show_data()->result();
		$this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
        $this->load->view('pelanggan_view', $data);
	}

	public function add_action()
	{
		$nama_pelanggan = $this->input->post('nama_pelanggan');
		$alamat = $this->input->post('alamat');
		$no_telp = $this->input->post('no_telp');
		$komentar = $this->input->post('komentar');
		$id_pelayanan = $this->input->post('id_pelayanan');

		$data = array(
			'nama_pelanggan' => $nama_pelanggan,
			'alamat' => $alamat,
			'no_telp' => $no_telp,
			'komentar' => $komentar,
			'id_pelayanan' => $id_pelayanan
		);

		$this->pelanggan_model->input_data($data, 'pelanggan');
		redirect('pelanggan_controller/index');
	}

	public function delete($id_pelanggan)
	{
		$where = array('id_pelanggan' => $id_pelanggan);
		$this->pelanggan_model->delete_data($where, 'pelanggan');
		redirect('pelanggan_controller/index');
	}

	public function edit($id_pelanggan)
	{
		$where = array('id_pelanggan' => $id_pelanggan);
		$data['pelanggan'] = $this->pelanggan_model->edit_data($where, 'pelanggan')->result();
		$this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
        $this->load->view('edit_pelanggan', $data);
	}

	public function update()
	{
		$id_pelanggan = $this->input->post('id_pelanggan');
		$nama_pelanggan = $this->input->post('nama_pelanggan');
		$alamat = $this->input->post('alamat');
		$no_telp = $this->input->post('no_telp');
		$komentar = $this->input->post('komentar');
		$id_pelayanan = $this->input->post('id_pelayanan');

		$data = array(
			'nama_pelanggan' => $nama_pelanggan,
			'alamat' => $alamat,
			'no_telp' => $no_telp,
			'komentar' => $komentar,
			'id_pelayanan' => $id_pelayanan
		);

		$where = array(
			'id_pelanggan' => $id_pelanggan
		);

		$this->pelanggan_model->update_data($where, $data, 'pelanggan');
		redirect('pelanggan_controller/index');
	}

	public function detail($id_pelanggan)
	{
		$this->load->model('pelanggan_model');
		$detail = $this->pelanggan_model->detail_data($id_pelanggan);
		$data['detail'] = $detail;
		$this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
        $this->load->view('detail_pelanggan', $data);
	}
}
