<?php

class Pelayanan_Controller extends CI_Controller {

	public function index()
	{
		$this->load->model('pelayanan_model');
		$data['pelayanan'] = $this->pelayanan_model->show_data()->result();
        $this->load->view('pelayanan_view', $data);
		$this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
	}

	public function add_action()
	{
		$jenis_pelayanan = $this->input->post('jenis_pelayanan');
		$harga = $this->input->post('harga');
		$waktu_pelayanan = $this->input->post('waktu_pelayanan');

		$data = array(
			'jenis_pelayanan' => $jenis_pelayanan,
			'harga' => $harga,
			'waktu_pelayanan' => $waktu_pelayanan
		);

		$this->pelayanan_model->input_data($data, 'pelayanan');
		redirect('pelayanan_controller/index');
	}

	public function delete($id_pelayanan)
	{
		$where = array('id_pelayanan' => $id_pelayanan);
		$this->pelayanan_model->delete_data($where, 'pelayanan');
		redirect('pelayanan_controller/index');
	}

	public function edit($id_pelayanan)
	{
		$where = array('id_pelayanan' => $id_pelayanan);
		$data['pelayanan'] = $this->pelayanan_model->edit_data($where, 'pelayanan')->result();
		$this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
        $this->load->view('edit_pelayanan', $data);
	}

	public function update()
	{
		$id_pelayanan = $this->input->post('id_pelayanan');
		$jenis_pelayanan = $this->input->post('jenis_pelayanan');
		$harga = $this->input->post('harga');
		$waktu_pelayanan = $this->input->post('waktu_pelayanan');

		$data = array(
			'jenis_pelayanan' => $jenis_pelayanan,
			'harga' => $harga,
			'waktu_pelayanan' => $waktu_pelayanan
		);

		$where = array(
			'id_pelayanan' => $id_pelayanan
		);

		$this->pelayanan_model->update_data($where, $data, 'pelayanan');
		redirect('pelayanan_controller/index');
	}

	public function detail($id_pelayanan)
	{
		$this->load->model('pelayanan_model');
		$detail = $this->pelayanan_model->detail_data($id_pelayanan);
		$data['detail'] = $detail;
		$this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
        $this->load->view('detail_pelayanan', $data);
	}
}
