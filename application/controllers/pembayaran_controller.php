<?php

class Pembayaran_Controller extends CI_Controller {

	public function index()
	{
		$this->load->model('pembayaran_model');
		$data['pembayaran'] = $this->pembayaran_model->show_data()->result();
        $this->load->view('pembayaran_view', $data);
		$this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
	}

	public function add_action()
	{
		$total_bayar = $this->input->post('total_bayar');
		$metode_pembayaran = $this->input->post('metode_pembayaran');
		$tanggal_pembayaran = $this->input->post('tanggal_pembayaran');
		$status_pembayaran = $this->input->post('status_pembayaran');
		$id_pelanggan = $this->input->post('id_pelanggan');

		$data = array(
			'total_bayar' => $total_bayar,
			'metode_pembayaran' => $metode_pembayaran,
			'tanggal_pembayaran' => $tanggal_pembayaran,
			'status_pembayaran' => $status_pembayaran,
			'id_pelanggan' => $id_pelanggan
		);

		$this->pembayaran_model->input_data($data, 'pembayaran');
		redirect('pembayaran_controller/index');
	}

	public function delete($id_pembayaran)
	{
		$where = array('id_pembayaran' => $id_pembayaran);
		$this->pembayaran_model->delete_data($where, 'pembayaran');
		redirect('pembayaran_controller/index');
	}

	public function edit($id_pembayaran)
	{
		$where = array('id_pembayaran' => $id_pembayaran);
		$data['pembayaran'] = $this->pembayaran_model->edit_data($where, 'pembayaran')->result();
		$this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
        $this->load->view('edit_pembayaran', $data);
	}

	public function update()
	{
		$id_pembayaran = $this->input->post('id_pembayaran');
		$total_bayar = $this->input->post('total_bayar');
		$metode_pembayaran = $this->input->post('metode_pembayaran');
		$tanggal_pembayaran = $this->input->post('tanggal_pembayaran');
		$status_pembayaran = $this->input->post('status_pembayaran');
		$id_pelanggan = $this->input->post('id_pelanggan');

		$data = array(
			'total_bayar' => $total_bayar,
			'metode_pembayaran' => $metode_pembayaran,
			'tanggal_pembayaran' => $tanggal_pembayaran,
			'status_pembayaran' => $status_pembayaran,
			'id_pelanggan' => $id_pelanggan
		);

		$where = array(
			'id_pembayaran' => $id_pembayaran
		);

		$this->pembayaran_model->update_data($where, $data, 'pembayaran');
		redirect('pembayaran_controller/index');

	}

	public function detail($id_pembayaran)
	{
		$this->load->model('pembayaran_model');
		$detail = $this->pembayaran_model->detail_data($id_pembayaran);
		$data['detail'] = $detail;
		$this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/footer');
        $this->load->view('detail_pembayaran', $data);
	}
}
